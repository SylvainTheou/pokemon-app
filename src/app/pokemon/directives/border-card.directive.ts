import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  constructor(private el: ElementRef) { 
    this.setHeight(this.defaultHeight);
    this.setBorder(this.initialColor);
  }

  private initialColor: string = '#f5f5f5';
  private defaultColor: string = '#c62828';
  private defaultHeight: number = 190;

  @Input('appBorderCard') borderColor: string; //alias 

  @HostListener('mouseenter') onMouseEnter(){
    this.setBorder(this.borderColor || this.defaultColor );
  }
  @HostListener('mouseleave') onMouseLeave(){
    this.setBorder(this.initialColor);
  }

  private setHeight(height: number){
    
    this.el.nativeElement.style.height = height +'px';
  }

  private setBorder(color: string){
    let border = 'solid 4px ' + color; 
    this.el.nativeElement.style.border = border;
  }


}
