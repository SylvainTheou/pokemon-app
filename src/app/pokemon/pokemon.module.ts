import { NgModule } from '@angular/core';
import { EditPokemonComponent } from './components/edit-pokemon/edit-pokemon.component';
import { AddPokemonComponent } from './components/add-pokemon/add-pokemon.component';
import { ListPokemonComponent } from './components/list-pokemon/list-pokemon.component';
import { DetailPokemonComponent } from './components/detail-pokemon/detail-pokemon.component';
import { BorderCardDirective } from './directives/border-card.directive';
import { PokemonTypeColorPipe } from './pipes/pokemon-type-color.pipe';
import { PokemonFormComponent } from './components/pokemon-form/pokemon-form.component';
import { SearchPokemonComponent } from './components/search-pokemon/search-pokemon.component';
import { LoaderComponent } from './components/loader/loader.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PokemonService } from './services/pokemon.service';
import { authGuard } from '../auth.guard';


const routes: Routes = [
  {path: 'edit/pokemon/:id', component: EditPokemonComponent, canActivate: [authGuard]},
  {path: 'pokemon/add', component: AddPokemonComponent, canActivate: [authGuard]},
  {path: 'pokemons', component: ListPokemonComponent, canActivate: [authGuard]},
  {path: 'pokemon/:id', component: DetailPokemonComponent, canActivate: [authGuard]},
  


];

@NgModule({
  declarations: [
    BorderCardDirective,
    PokemonTypeColorPipe,
    ListPokemonComponent,
    DetailPokemonComponent,
    PokemonFormComponent,
    EditPokemonComponent,
    AddPokemonComponent,
    SearchPokemonComponent,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes), 
  ],
  providers: [PokemonService]
})
export class PokemonModule { }
