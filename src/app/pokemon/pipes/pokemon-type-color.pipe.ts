import { Pipe, PipeTransform } from '@angular/core';

/*
 * Affiche la couleur correspondant au type du pokémon.
 * Prend en argument le type du pokémon.
 * Exemple d'utilisation:
 *   {{ pokemon.type | pokemonTypeColor }}
*/
@Pipe({name: 'pokemonTypeColor'})
export class PokemonTypeColorPipe implements PipeTransform {
  transform(type: string): string {
  
    let color: string;
  
    switch (type) {
      case 'Acier':
        color = 'blue-grey lighten-1';
        break;
      case 'Combat':
        color = 'orange lighten-1';
        break;
      case 'Dragon':
        color = 'indigo lighten-1';
        break;
      case 'Eau':
        color = 'light-blue lighten-1';
        break;
      case 'Electrik':
        color = 'amber lighten-2';
        break;
      case 'Fée':
        color = 'purple accent-1';
        break;
      case 'Feu':
        color = 'red lighten-1';
        break;
      case 'Glace':
        color = 'cyan lighten-1';
        break;
      case 'Insecte':
        color = 'light-green lighten-1';
        break;
      case 'Normal':
        color = 'grey lighten-1';
        break;
      case 'Plante':
        color = 'green lighten-1';
        break;
      case 'Poison':
        color = 'purple lighten-1';
        break;
      case 'Psy':
        color = 'pink lighten-1';
        break;
      case 'Roche':
        color = 'brown lighten-4';
        break;
      case 'Sol':
        color = 'brown lighten-1';
        break;
      case 'Spectre':
        color = 'deep-purple darken-3';
        break;
      case 'Tenebres':
        color = 'brown darken-4';
        break;
      case 'Vol':
        color = 'blue lighten-4';
        break;
      default:
        color = 'grey';
        break;
    }
  
    return 'chip ' + color;
  
  }
}
