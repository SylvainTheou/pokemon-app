import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, of, tap, Observable } from 'rxjs';

@Injectable()
export class PokemonService {

  constructor(
    private http: HttpClient,

  ) { }

  getPokemonList(): Observable<Pokemon[]>{
    //requête http get, on va recevoir un observable. On spécifie que la réponse contient une liste de pokémons.
    //en paramètre de la méthode Get on passe l'URL de notre serveur de données.
    return this.http.get<Pokemon[]>('api/pokemons').pipe(  
      //A chaque réponse, on va logué la réponse
      //tap = console log pour les observables
      tap((response)=>this.log(response)),
      //si erreur, on log l'erreur et on retourne un tableau vide pour eviter que l'application plante.
      catchError((error)=> this.handleError(error, []))
    );
  }

  getPokemonById(pokemonId: number): Observable<Pokemon | undefined>{
    return this.http.get<Pokemon>(`api/pokemons/${pokemonId}`).pipe(
      tap((response)=>this.log(response)),
      catchError((error)=> this.handleError(error, undefined))
    );
  }

  updatePokemon(pokemon: Pokemon): Observable<null>{
    const httpOption = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    return this.http.put('api/pokemons', pokemon, httpOption).pipe(
      tap((response)=>this.log(response)),
      catchError((error)=> this.handleError(error, null))
    );
  }

  deletePokemonById(pokemonId: number): Observable<null>{
    return this.http.delete(`api/pokemons/${pokemonId}`).pipe(
      tap((response)=>this.log(response)),
      catchError((error)=> this.handleError(error, null))
    );
  }

  addPokemon(pokemon: Pokemon): Observable<Pokemon>{
    const httpOption = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    return this.http.post<Pokemon>('api/pokemons', pokemon, httpOption).pipe(
      tap((response)=>this.log(response)),
      catchError((error)=> this.handleError(error, null))
    );
  }


  private log(response: any){
    console.table(response);
  }
  private handleError(error: Error, errorValue: any){
    console.log(error);
    return of(errorValue);
  }

  searchPokemonList(term: string): Observable<Pokemon[]>{
    if(term.length <=1) {
      return of([]);
    }else{
    return this.http.get<Pokemon>(`api/pokemons/?name=${term}`)
      .pipe(
        tap((response)=>this.log(response)),
        catchError((error)=> this.handleError(error, []))
      )
    }
  }


  getPokemonTypeList(): string[] {
    return [
      'Plante',
      'Feu',
      'Eau',
      'Normal',
      'Insecte',
      'Psy',
      'Fée',
      'Poison',
      'Combat',
      'Electrik',
      'Vol',
      'Acier',
      'Dragon',
      'Roche',
      'Sol',
      'Ténèbre',
      'Glace',
      'Spectre'
    ];
  }
}
