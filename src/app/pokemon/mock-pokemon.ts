/* Chapitre 2 : Les Composants */
import { Pokemon } from './models/pokemon';
  
export const POKEMONS: Pokemon[] = [
    {
        id: 1,
        name: "Bulbizarre",
        hp: 45,
        cp: 49,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 2,
        name: "Herbizarre",
        hp: 60,
        cp: 62,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 3,
        name: "Florizarre",
        hp: 80,
        cp: 82,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/003.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 4,
        name: "Salamèche",
        hp: 39,
        cp: 52,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 5,
        name: "Reptincel",
        hp: 58,
        cp: 64,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/005.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 6,
        name: "Dracaufeu",
        hp: 78,
        cp: 84,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png",
        types: ["Feu", "Vol"],
        created: new Date()
    },
    {
        id: 7,
        name: "Carapuce",
        hp: 44,
        cp: 48,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 8,
        name: "Carabaffe",
        hp: 59,
        cp: 63,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/008.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 9,
        name: "Tortank",
        hp: 79,
        cp: 83,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/009.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 10,
        name: "Chenipan",
        hp: 45,
        cp: 30,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/010.png",
        types: ["Insecte"],
        created: new Date()
    },
    {
        id: 11,
        name: "Chrysacier",
        hp: 50,
        cp: 20,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/011.png",
        types: ["Insecte"],
        created: new Date()
    },
    {
        id: 12,
        name: "Papilusion",
        hp: 60,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/012.png",
        types: ["Insecte", "Vol"],
        created: new Date()
    },
    {
        id: 13,
        name: "Aspicot",
        hp: 40,
        cp: 35,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/013.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 14,
        name: "Coconfort",
        hp: 45,
        cp: 25,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/014.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 15,
        name: "Dardargnan",
        hp: 65,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/015.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 16,
        name: "Roucool",
        hp: 40,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/016.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 17,
        name: "Roucoups",
        hp: 63,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/017.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 18,
        name: "Roucarnage",
        hp: 83,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/018.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 19,
        name: "Rattata",
        hp: 30,
        cp: 56,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/019.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 20,
        name: "Rattatac",
        hp: 55,
        cp: 81,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/020.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 21,
        name: "Piafabec",
        hp: 40,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/021.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 22,
        name: "Rapasdepic",
        hp: 65,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/022.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 23,
        name: "Abo",
        hp: 35,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/023.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 24,
        name: "Arbok",
        hp: 60,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/024.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 25,
        name: "Pikachu",
        hp: 35,
        cp: 55,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/025.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 26,
        name: "Raichu",
        hp: 60,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/026.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 27,
        name: "Sabelette",
        hp: 50,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/027.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 28,
        name: "Sablaireau",
        hp: 75,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/028.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 29,
        name: "Nidoran♀",
        hp: 55,
        cp: 47,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/029.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 30,
        name: "Nidorina",
        hp: 70,
        cp: 62,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/030.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 31,
        name: "Nidoqueen",
        hp: 90,
        cp: 92,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/031.png",
        types: ["Poison", "Sol"],
        created: new Date()
    },
    {
        id: 32,
        name: "Nidoran♂",
        hp: 46,
        cp: 57,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/032.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 33,
        name: "Nidorino",
        hp: 61,
        cp: 72,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/033.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 34,
        name: "Nidoking",
        hp: 81,
        cp: 102,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/034.png",
        types: ["Poison", "Sol"],
        created: new Date()
    },
    {
        id: 35,
        name: "Mélofée",
        hp: 70,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/035.png",
        types: ["Fée"],
        created: new Date()
    },
    {
        id: 36,
        name: "Mélodelfe",
        hp: 95,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/036.png",
        types: ["Fée"],
        created: new Date()
    },
    {
        id: 37,
        name: "Goupix",
        hp: 55,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/037.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 38,
        name: "Feunard",
        hp: 73,
        cp: 76,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/038.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 39,
        name: "Rondoudou",
        hp: 115,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/039.png",
        types: ["Normal", "Fée"],
        created: new Date()
    },
    {
        id: 40,
        name: "Grodoudou",
        hp: 140,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/040.png",
        types: ["Normal", "Fée"],
        created: new Date()
    },
    {
        id: 41,
        name: "Nosferapti",
        hp: 40,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/041.png",
        types: ["Poison", "Vol"],
        created: new Date()
    },
    {
        id: 42,
        name: "Nosferalto",
        hp: 75,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/042.png",
        types: ["Poison", "Vol"],
        created: new Date()
    },
    {
        id: 43,
        name: "Mystherbe",
        hp: 45,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/043.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 44,
        name: "Ortide",
        hp: 60,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/044.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 45,
        name: "Rafflesia",
        hp: 75,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/045.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 46,
        name: "Paras",
        hp: 35,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/046.png",
        types: ["Insecte", "Plante"],
        created: new Date()
    },
    {
        id: 47,
        name: "Parasect",
        hp: 60,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/047.png",
        types: ["Insecte", "Plante"],
        created: new Date()
    },
    {
        id: 48,
        name: "Mimitoss",
        hp: 60,
        cp: 55,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/048.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 49,
        name: "Aéromite",
        hp: 70,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/049.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 50,
        name: "Taupiqueur",
        hp: 10,
        cp: 55,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/050.png",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 51,
        name: "Triopikeur",
        hp: 35,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/051.png",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 52,
        name: "Miaouss",
        hp: 40,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/052.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 53,
        name: "Persian",
        hp: 65,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/053.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 54,
        name: "Psykokwak",
        hp: 50,
        cp: 52,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/054.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 55,
        name: "Akwakwak",
        hp: 80,
        cp: 82,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/055.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 56,
        name: "Férosinge",
        hp: 40,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/056.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 57,
        name: "Colossinge",
        hp: 65,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/057.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 58,
        name: "Caninos",
        hp: 55,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/058.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 59,
        name: "Arcanin",
        hp: 90,
        cp: 110,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/059.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 60,
        name: "Ptitard",
        hp: 40,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/060.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 61,
        name: "Têtarte",
        hp: 65,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/061.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 62,
        name: "Tartard",
        hp: 90,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/062.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 63,
        name: "Abra",
        hp: 25,
        cp: 20,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/063.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 64,
        name: "Kadabra",
        hp: 40,
        cp: 35,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/064.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 65,
        name: "Alakazam",
        hp: 55,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/065.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 66,
        name: "Machoc",
        hp: 70,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/066.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 67,
        name: "Machopeur",
        hp: 80,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/067.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 68,
        name: "Mackogneur",
        hp: 90,
        cp: 130,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/068.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 69,
        name: "Chétiflor",
        hp: 45,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/069.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 70,
        name: "Boustiflor",
        hp: 60,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/070.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 71,
        name: "Empiflor",
        hp: 75,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/071.png",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 72,
        name: "Tentacool",
        hp: 40,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/072.png",
        types: ["Eau", "Poison"],
        created: new Date()
    },
    {
        id: 73,
        name: "Tentacruel",
        hp: 80,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/073.png",
        types: ["Eau", "Poison"],
        created: new Date()
    },
    {
        id: 74,
        name: "Racaillou",
        hp: 40,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/074.png",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 75,
        name: "Gravalanch",
        hp: 55,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/075.png",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 76,
        name: "Grolem",
        hp: 80,
        cp: 120,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/076.png",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 77,
        name: "Ponyta",
        hp: 50,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/077.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 78,
        name: "Galopa",
        hp: 65,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/078.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 79,
        name: "Ramoloss",
        hp: 90,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/079.png",
        types: ["Eau", "Psy"],
        created: new Date()
    },
    {
        id: 80,
        name: "Flagadoss",
        hp: 95,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/080.png",
        types: ["Eau", "Psy"],
        created: new Date()
    },
    {
        id: 81,
        name: "Magnéti",
        hp: 25,
        cp: 35,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/081.png",
        types: ["Electrik", "Acier"],
        created: new Date()
    },
    {
        id: 82,
        name: "Magnéton",
        hp: 50,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/082.png",
        types: ["Electrik", "Acier"],
        created: new Date()
    },
    {
        id: 83,
        name: "Canarticho",
        hp: 52,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/083.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 84,
        name: "Doduo",
        hp: 35,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/084.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 85,
        name: "Dodrio",
        hp: 60,
        cp: 110,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/085.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 86,
        name: "Otaria",
        hp: 65,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/086.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 87,
        name: "Lamantine",
        hp: 90,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/087.png",
        types: ["Eau", "Glace"],
        created: new Date()
    },
    {
        id: 88,
        name: "Tadmorv",
        hp: 80,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/088.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 89,
        name: "Grotadmorv",
        hp: 105,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/089.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 90,
        name: "Kokiyas",
        hp: 30,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/090.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 91,
        name: "Crustabri",
        hp: 50,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/091.png",
        types: ["Eau", "Glace"],
        created: new Date()
    },
    {
        id: 92,
        name: "Fantominus",
        hp: 30,
        cp: 35,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/092.png",
        types: ["Spectre", "Poison"],
        created: new Date()
    },
    {
        id: 93,
        name: "Spectrum",
        hp: 45,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/093.png",
        types: ["Spectre", "Poison"],
        created: new Date()
    },
    {
        id: 94,
        name: "Ectoplasma",
        hp: 60,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/094.png",
        types: ["Spectre", "Poison"],
        created: new Date()
    },
    {
        id: 95,
        name: "Onix",
        hp: 35,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/095.png",
        types: ["Roche", "Sol"],
        created: new Date()
    },
    {
        id: 96,
        name: "Soporifik",
        hp: 60,
        cp: 48,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/096.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 97,
        name: "Hypnomade",
        hp: 85,
        cp: 73,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/097.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 98,
        name: "Krabby",
        hp: 30,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/098.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 99,
        name: "Krabboss",
        hp: 55,
        cp: 130,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/099.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 100,
        name: "Voltorbe",
        hp: 40,
        cp: 30,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/100.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 101,
        name: "Électrode",
        hp: 60,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/101.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 102,
        name: "Noeunoeuf",
        hp: 60,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/102.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 103,
        name: "Noadkoko",
        hp: 95,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/103.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 104,
        name: "Osselait",
        hp: 50,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/104.png",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 105,
        name: "Ossatueur",
        hp: 60,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/105.png",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 106,
        name: "Kicklee",
        hp: 50,
        cp: 120,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/106.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 107,
        name: "Tygnon",
        hp: 50,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/107.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 108,
        name: "Excelangue",
        hp: 90,
        cp: 55,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/108.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 109,
        name: "Smogo",
        hp: 40,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/109.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 110,
        name: "Smogogo",
        hp: 65,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/110.png",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 111,
        name: "Rhinocorne",
        hp: 80,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/111.png",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 112,
        name: "Rhinoféros",
        hp: 105,
        cp: 130,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/112.png",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 113,
        name: "Leveinard",
        hp: 250,
        cp: 5,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/113.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 114,
        name: "Saquedeneu",
        hp: 60,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/114.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 115,
        name: "Kangourex",
        hp: 105,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/115.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 116,
        name: "Hypotrempe",
        hp: 30,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/116.png",
        types: ["Eau", "Dragon"],
        created: new Date()
    },
    {
        id: 117,
        name: "Hypocéan",
        hp: 55,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/117.png",
        types: ["Eau", "Dragon"],
        created: new Date()
    },
    {
        id: 118,
        name: "Poissirène",
        hp: 45,
        cp: 67,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/118.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 119,
        name: "Poissoroy",
        hp: 80,
        cp: 92,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/119.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 120,
        name: "Stari",
        hp: 30,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/120.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 121,
        name: "Staross",
        hp: 60,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/121.png",
        types: ["Eau", "Psy"],
        created: new Date()
    },
    {
        id: 122,
        name: "Mr. Mime",
        hp: 40,
        cp: 45,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/122.png",
        types: ["Psy", "Fée"],
        created: new Date()
    },
    {
        id: 123,
        name: "Insécateur",
        hp: 70,
        cp: 110,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/123.png",
        types: ["Insecte", "Vol"],
        created: new Date()
    },
    {
        id: 124,
        name: "Lippoutou",
        hp: 65,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/124.png",
        types: ["Glace", "Psy"],
        created: new Date()
    },
    {
        id: 125,
        name: "Élektek",
        hp: 65,
        cp: 83,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/125.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 126,
        name: "Magmar",
        hp: 65,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/126.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 127,
        name: "Scarabrute",
        hp: 70,
        cp: 130,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/127.png",
        types: ["Insecte", "Sol"],
        created: new Date()
    },
    {
        id: 128,
        name: "Tauros",
        hp: 75,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/128.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 129,
        name: "Magicarpe",
        hp: 20,
        cp: 10,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/129.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 130,
        name: "Léviator",
        hp: 95,
        cp: 125,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/130.png",
        types: ["Eau", "Vol"],
        created: new Date()
    },
    {
        id: 131,
        name: "Lokhlass",
        hp: 130,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/131.png",
        types: ["Eau", "Glace"],
        created: new Date()
    },
    {
        id: 132,
        name: "Métamorph",
        hp: 48,
        cp: 48,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/132.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 133,
        name: "Évoli",
        hp: 55,
        cp: 55,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/133.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 134,
        name: "Aquali",
        hp: 130,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/134.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 135,
        name: "Voltali",
        hp: 130,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/135.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 136,
        name: "Pyroli",
        hp: 130,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/136.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 137,
        name: "Porygon",
        hp: 65,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/137.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 138,
        name: "Amonita",
        hp: 35,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/138.png",
        types: ["Roche", "Eau"],
        created: new Date()
    },
    {
        id: 139,
        name: "Amonistar",
        hp: 70,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/139.png",
        types: ["Roche", "Eau"],
        created: new Date()
    },
    {
        id: 140,
        name: "Kabuto",
        hp: 30,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/140.png",
        types: ["Roche", "Eau"],
        created: new Date()
    },
    {
        id: 141,
        name: "Kabutops",
        hp: 60,
        cp: 115,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/141.png",
        types: ["Roche", "Eau"],
        created: new Date()
    },
    {
        id: 142,
        name: "Ptéra",
        hp: 80,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/142.png",
        types: ["Roche", "Vol"],
        created: new Date()
    },
    {
        id: 143,
        name: "Ronflex",
        hp: 160,
        cp: 110,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/143.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 144,
        name: "Artikodin",
        hp: 90,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/144.png",
        types: ["Glace", "Vol"],
        created: new Date()
    },
    {
        id: 145,
        name: "Électhor",
        hp: 90,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/145.png",
        types: ["Electrik", "Vol"],
        created: new Date()
    },
    {
        id: 146,
        name: "Sulfura",
        hp: 90,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/146.png",
        types: ["Feu", "Vol"],
        created: new Date()
    },
    {
        id: 147,
        name: "Minidraco",
        hp: 41,
        cp: 64,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/147.png",
        types: ["Dragon"],
        created: new Date()
    },
    {
        id: 148,
        name: "Draco",
        hp: 61,
        cp: 84,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/148.png",
        types: ["Dragon"],
        created: new Date()
    },
    {
        id: 149,
        name: "Dracolosse",
        hp: 91,
        cp: 134,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/149.png",
        types: ["Dragon", "Vol"],
        created: new Date()
    },
    {
        id: 150,
        name: "Mewtwo",
        hp: 106,
        cp: 110,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/150.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 151,
        name: "Mew",
        hp: 100,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/151.png",
        types: ["Psy"],
        created: new Date()
    }, 
    {
        id: 152,
        name: "Germignon",
        hp: 45,
        cp: 49,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/152.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 153,
        name: "Macronium",
        hp: 60,
        cp: 62,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/153.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 154,
        name: "Méganium",
        hp: 80,
        cp: 82,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/154.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 155,
        name: "Héricendre",
        hp: 39,
        cp: 52,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/155.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 156,
        name: "Feurisson",
        hp: 58,
        cp: 64,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/156.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 157,
        name: "Typhlosion",
        hp: 78,
        cp: 84,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/157.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 158,
        name: "Kaiminus",
        hp: 50,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/158.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 159,
        name: "Crocrodil",
        hp: 65,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/159.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 160,
        name: "Aligatueur",
        hp: 85,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/160.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 161,
        name: "Fouinette",
        hp: 35,
        cp: 46,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/161.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 162,
        name: "Fouinar",
        hp: 85,
        cp: 76,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/162.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 163,
        name: "Hoothoot",
        hp: 60,
        cp: 30,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/163.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 164,
        name: "Noarfang",
        hp: 100,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/164.png",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 165,
        name: "Coxy",
        hp: 40,
        cp: 20,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/165.png",
        types: ["Insecte"],
        created: new Date()
    },
    {
        id: 166,
        name: "Coxyclaque",
        hp: 75,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/166.png",
        types: ["Insecte"],
        created: new Date()
    },
    {
        id: 167,
        name: "Mimigal",
        hp: 40,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/167.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 168,
        name: "Migalos",
        hp: 70,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/168.png",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 169,
        name: "Nostenfer",
        hp: 85,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/169.png",
        types: ["Poison", "Vol"],
        created: new Date()
    },
    {
        id: 170,
        name: "Loupio",
        hp: 55,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/170.png",
        types: ["Eau", "Électrik"],
        created: new Date()
    },
    {
        id: 171,
        name: "Lanturn",
        hp: 125,
        cp: 67,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/171.png",
        types: ["Eau", "Électrik"],
        created: new Date()
    },
    {
        id: 172,
        name: "Pichu",
        hp: 20,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/172.png",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 173,
        name: "Mélo",
        hp: 100,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/173.png",
        types: ["Fée"],
        created: new Date()
    },
    {
        id: 174,
        name: "Toudoudou",
        hp: 90,
        cp: 30,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/174.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 175,
        name: "Togepi",
        hp: 35,
        cp: 20,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/175.png",
        types: ["Fée"],
        created: new Date()
    },
    {
        id: 176,
        name: "Togetic",
        hp: 55,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/176.png",
        types: ["Fée", "Vol"],
        created: new Date()
    },
    {
        id: 177,
        name: "Natu",
        hp: 40,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/177.png",
        types: ["Psy", "Vol"],
        created: new Date()
    },
    {
        id: 191,
        name: "Tournegrin",
        hp: 55,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/191.png",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 192,
        name: "Héliatronc",
        hp: 85,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/192.png",
        types: ["Plante", "Vol"],
        created: new Date()
    },
    {
        id: 193,
        name: "Yanma",
        hp: 65,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/193.png",
        types: ["Insecte", "Vol"],
        created: new Date()
    },
    {
        id: 194,
        name: "Axoloto",
        hp: 35,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/194.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 195,
        name: "Maraiste",
        hp: 75,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/195.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 196,
        name: "Mentali",
        hp: 65,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/196.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 197,
        name: "Noctali",
        hp: 65,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/197.png",
        types: ["Ténèbres"],
        created: new Date()
    },
    {
        id: 198,
        name: "Cornebre",
        hp: 60,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/198.png",
        types: ["Ténèbres", "Vol"],
        created: new Date()
    },
    {
        id: 199,
        name: "Roigada",
        hp: 95,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/199.png",
        types: ["Ténèbres", "Vol"],
        created: new Date()
    },
    {
        id: 200,
        name: "Feuforêve",
        hp: 50,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/200.png",
        types: ["Spectre"],
        created: new Date()
    },
    {
        id: 201,
        name: "Zarbi",
        hp: 48,
        cp: 48,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/201.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 202,
        name: "Qulbutoké",
        hp: 255,
        cp: 10,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/202.png",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 203,
        name: "Girafarig",
        hp: 70,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/203.png",
        types: ["Normal", "Psy"],
        created: new Date()
    },
    {
        id: 204,
        name: "Pomdepik",
        hp: 50,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/204.png",
        types: ["Électrik"],
        created: new Date()
    },
    {
        id: 205,
        name: "Foretress",
        hp: 75,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/205.png",
        types: ["Insecte", "Acier"],
        created: new Date()
    },
    {
        id: 206,
        name: "Insolourdo",
        hp: 55,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/206.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 207,
        name: "Scorplane",
        hp: 70,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/207.png",
        types: ["Vol", "Insecte"],
        created: new Date()
    },
    {
        id: 208,
        name: "Steelix",
        hp: 75,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/208.png",
        types: ["Acier", "Sol"],
        created: new Date()
    },
    {
        id: 209,
        name: "Snubbull",
        hp: 60,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/209.png",
        types: ["Fée"],
        created: new Date()
    },
    {
        id: 210,
        name: "Granbull",
        hp: 90,
        cp: 120,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/210.png",
        types: ["Fée"],
        created: new Date()
    },
    {
        id: 211,
        name: "Qwilfish",
        hp: 65,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/211.png",
        types: ["Eau", "Poison"],
        created: new Date()
    },
    {
        id: 212,
        name: "Cizayox",
        hp: 65,
        cp: 130,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/212.png",
        types: ["Insecte", "Acier"],
        created: new Date()
    },
    {
        id: 213,
        name: "Caratroc",
        hp: 70,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/213.png",
        types: ["Roche", "Eau"],
        created: new Date()
    },
    {
        id: 214,
        name: "Scarhino",
        hp: 70,
        cp: 120,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/214.png",
        types: ["Insecte", "Combat"],
        created: new Date()
    },
    {
        id: 215,
        name: "Farfuret",
        hp: 85,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/215.png",
        types: ["Ténèbres", "Glace"],
        created: new Date()
    },
    {
        id: 216,
        name: "Teddiursa",
        hp: 60,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/216.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 217,
        name: "Ursaring",
        hp: 90,
        cp: 130,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/217.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 218,
        name: "Limagma",
        hp: 50,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/218.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 219,
        name: "Volcaropod",
        hp: 70,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/219.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 220,
        name: "Marcacrin",
        hp: 100,
        cp: 30,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/220.png",
        types: ["Eau", "Glace"],
        created: new Date()
    },
    {
        id: 221,
        name: "Cochignon",
        hp: 100,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/221.png",
        types: ["Eau", "Glace"],
        created: new Date()
    },
    {
        id: 222,
        name: "Corayon",
        hp: 100,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/222.png",
        types: ["Eau", "Roche"],
        created: new Date()
    },
    {
        id: 223,
        name: "Rémoraid",
        hp: 35,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/223.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 224,
        name: "Octillery",
        hp: 75,
        cp: 105,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/224.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 225,
        name: "Cadoizo",
        hp: 75,
        cp: 115,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/225.png",
        types: ["Eau", "Vol"],
        created: new Date()
    },
    {
        id: 226,
        name: "Démanta",
        hp: 65,
        cp: 40,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/226.png",
        types: ["Eau", "Vol"],
        created: new Date()
    },
    {
        id: 227,
        name: "Airmure",
        hp: 65,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/227.png",
        types: ["Acier", "Vol"],
        created: new Date()
    },
    {
        id: 228,
        name: "Malosse",
        hp: 70,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/228.png",
        types: ["Ténèbres", "Feu"],
        created: new Date()
    },
    {
        id: 229,
        name: "Démolosse",
        hp: 70,
        cp: 110,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/229.png",
        types: ["Ténèbres", "Feu"],
        created: new Date()
    },
    {
        id: 230,
        name: "Hyporoi",
        hp: 85,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/230.png",
        types: ["Eau", "Dragon"],
        created: new Date()
    },
    {
        id: 231,
        name: "Phanpy",
        hp: 90,
        cp: 60,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/231.png",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 232,
        name: "Donphan",
        hp: 90,
        cp: 120,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/232.png",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 233,
        name: "Porygon2",
        hp: 85,
        cp: 80,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/233.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 234,
        name: "Cerfrousse",
        hp: 100,
        cp: 50,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/234.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 235,
        name: "Queulorior",
        hp: 255,
        cp: 10,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/235.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 236,
        name: "Debugant",
        hp: 35,
        cp: 70,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/236.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 237,
        name: "Kapoera",
        hp: 50,
        cp: 95,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/237.png",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 238,
        name: "Lippouti",
        hp: 85,
        cp: 65,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/238.png",
        types: ["Glace", "Psy"],
        created: new Date()
    },
    {
        id: 239,
        name: "Élekid",
        hp: 45,
        cp: 63,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/239.png",
        types: ["Électrik"],
        created: new Date()
    },
    {
        id: 240,
        name: "Magby",
        hp: 45,
        cp: 75,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/240.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 241,
        name: "Écrémeuh",
        hp: 90,
        cp: 30,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/241.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 242,
        name: "Leuphorie",
        hp: 255,
        cp: 10,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/242.png",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 243,
        name: "Raikou",
        hp: 90,
        cp: 85,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/243.png",
        types: ["Électrik"],
        created: new Date()
    },
    {
        id: 244,
        name: "Entei",
        hp: 115,
        cp: 115,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/244.png",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 245,
        name: "Suicune",
        hp: 100,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/245.png",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 246,
        name: "Embrylex",
        hp: 50,
        cp: 64,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/246.png",
        types: ["Roche", "Sol"],
        created: new Date()
    },
    {
        id: 247,
        name: "Ymphect",
        hp: 70,
        cp: 84,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/247.png",
        types: ["Roche", "Sol"],
        created: new Date()
    },
    {
        id: 248,
        name: "Tyranocif",
        hp: 100,
        cp: 134,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/248.png",
        types: ["Roche", "Ténèbres"],
        created: new Date()
    },
    {
        id: 249,
        name: "Lugia",
        hp: 106,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/249.png",
        types: ["Psy", "Vol"],
        created: new Date()
    },
    {
        id: 250,
        name: "Ho-Oh",
        hp: 106,
        cp: 90,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/250.png",
        types: ["Feu", "Vol"],
        created: new Date()
    },
    {
        id: 251,
        name: "Celebi",
        hp: 100,
        cp: 100,
        picture: "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/251.png",
        types: ["Plante", "Psy"],
        created: new Date()
    }  
];