import { Component, OnInit } from "@angular/core";
import { Pokemon } from "../../models/pokemon";
import { PokemonService } from "../../services/pokemon.service";

@Component({
  selector: "app-list-pokemon",
  templateUrl: "./list-pokemon.component.html",
  styleUrls: ["./list-pokemon.component.css"],
})
export class ListPokemonComponent implements OnInit {
  // ngAfterViewInit() {
  //   // const elems = document.querySelectorAll('.fixed-action-btn');

  //   // const instances = M.FloatingActionButton.init(elems, {
  //   //   direction: 'top'}
  //   //   );
  // }

  pokemonList: Pokemon[];

  constructor(private pokemonService: PokemonService) {}

  ngOnInit() {
    this.pokemonService
      .getPokemonList()
      .subscribe((pokemonList) => (this.pokemonList = pokemonList));
  }
}
