import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../models/pokemon';
import { Router } from '@angular/router';
import { Observable, Subject, debounce, debounceTime, distinctUntilChanged, switchMap } from 'rxjs';
import { PokemonService } from '../../services/pokemon.service';

@Component({
  selector: 'app-search-pokemon',
  templateUrl: './search-pokemon.component.html',
  styles: [
  ]
})
export class SearchPokemonComponent implements OnInit{
  //{..."a".."ab".."abz".."ab"..."abc"...} Subject : construire un flux de données dans le temps.
  searchTerms = new Subject<string>(); 
  // pokemon list (a) pokemonList rechercher ab
  pokemon$: Observable<Pokemon[]>;


  constructor(
    private router: Router,
    private pokemonService: PokemonService,

  ){

  }
  ngOnInit()  {
    this.pokemon$ = this.searchTerms.pipe(
      //{"a"."ab"..."abz"."ab"..."abc"...}
      debounceTime(300),
      //attendre 300ms avant de déranger le serveur
      //{"ab"..."ab"..."abc".....}
      distinctUntilChanged(),
      //{"ab".........."abc".....}
      switchMap((term) => this.pokemonService.searchPokemonList(term))
      //{Observable<ab>..........Observable<abc>.....}
      // concatMap/mergeMap/SwitchMap
    );
  }

  search(term: string){
    this.searchTerms.next(term);
  }

  goToDetail(pokemon: Pokemon){
    const link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
  }
}
