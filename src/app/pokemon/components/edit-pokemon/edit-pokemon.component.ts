import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PokemonService } from "../../services/pokemon.service";
import { Pokemon } from "../../models/pokemon";

@Component({
  selector: "app-edit-pokemon",
  template: `
    <ng-container *ngIf="pokemon; else noPokemon">
      <div class="center">
        <h2>Éditer {{ pokemon.name }}</h2>
        <img [src]="pokemon.picture" />
      </div>
      <app-pokemon-form *ngIf="pokemon" [pokemon]="pokemon"></app-pokemon-form>
    </ng-container>
    <ng-template #noPokemon>
      <div class="center">
        <app-loader></app-loader>
      </div>
    </ng-template>
  `,
  styleUrls: [],
})
export class EditPokemonComponent implements OnInit {
  pokemon: Pokemon | undefined;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ) {}

  ngOnInit(): void {
    const pokemonId: string | null = this.route.snapshot.paramMap.get("id");
    if (pokemonId) {
      this.pokemonService
        .getPokemonById(+pokemonId)
        .subscribe((pokemon) => (this.pokemon = pokemon));
    } else {
      this.pokemon = undefined;
    }
  }
}
